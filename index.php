<?php

error_reporting(0);
session_start();
include("captcha/simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();

?>

<!DOCTYPE html>

<html lang="de">

<head>
  <meta charset="utf-8">
  <meta name="apple-mobile-web-app-capable" content="yes"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no"/>

  <meta name="description" content="Dipl.-Pianistin und Dipl.-Musikpädagogin aus Dresden. Klaviermusik für Ihre Veranstaltung, Ihr Event, Unterricht oder Konzert."/>
  <meta name="abstract" content="Dipl.-Pianistin und Dipl.-Musikpädagogin aus Dresden. Klaviermusik für Ihre Veranstaltung, Ihr Event, Unterricht oder Konzert."/>
  <meta name="keywords" content="Pianistin, Pianist, Musikerin, Musiker,  Hochzeit, Trauung, Vernissage, Barpianist, Klavierunterricht, musikalische Umrahmung, Hintergrundmusik, Beerdigung, Dresden, Klaviermusik, Klavier, Klavierspieler, Klavierspielerin, Musik, Piano, Künstler, Künstlerin, Veranstaltung, Konzert, Auftritt, buchen, engagieren, Solist, Solistin, Event, Klavierlehrerin, Pädagogin"/>
  <meta name="page-topic" content="Musik"/>
  <meta name="author" content="Walentina Wachtel - Pianistin">
  <meta name="Publisher" content="Walentina Wachtel - Pianistin">
  <meta name="Copyright" content="Walentina Wachtel - Pianistin">
  <meta name="Revisit" content="After 7 days"/>

  <!-- STYLES -->
  <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
  <title>Walentina Wachtel - Pianistin</title>
  <link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="images/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
  <link rel="manifest" href="images/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="images/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
</head>

<body>

<header class="left-menu mainmenu">
  <div class="container no-padd-h">
    <div class="navigation">
      <nav class="">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Navigation ein-/ausblenden</span> <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <a class="logo" href="#"><img src="images/logox.png" class="img-responsive" alt='Walentina Wachtel'></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse no-padd-h">
            <ul class="menu">
              <li class="menu-item page-scroll"><a class="anchor-scroll" href="#image_background">Home</a>
              </li>
              <li class="menu-item page-scroll"><a class="anchor-scroll" href="#about-us">Vita</a></li>
              <li class="menu-item page-scroll"><a class="anchor-scroll" href="#portfolio">Bilder</a></li>
              <li class="menu-item page-scroll"><a class="anchor-scroll" href="#why-we">Audio</a></li>
              <li class="menu-item page-scroll"><a class="anchor-scroll" href="#services">Klavierunterricht</a></li>
              <li class="menu-item page-scroll"><a class="anchor-scroll" href="#testimonials">Duo WALIA</a></li>
              <li class="menu-item page-scroll"><a class="anchor-scroll" href="#contact-us">Kontakt</a>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
    </div>
  </div>
</header>

<div id="loading">
  <div id="loading-center">
    <div id="loading-center-absolute">
      <div class="cssload-wrap">
        <div class="cssload-cssload-spinner"></div>
      </div>
    </div>
  </div>
</div>

<div id="image_background" class="container-fluid no-padd">
  <div class="row no-marg">
    <div class="col-sm-12 no-padd">
      <div class="top-baner swiper-anime">
        <div class="swiper-container top-slider-2 style-3" data-autoplay="0" data-loop="0" data-speed="0" data-center="0" data-slides-per-view="1">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="bg-main">
                <div class="clip">
                  <div class="bg bg-bg">
                    <img src="images/img.jpg" class="anim-img s-img-switch" alt="">
                  </div>
                  <div class="dark-layer"></div>
                </div>
              </div>

            </div>
          </div>
          <div class="container">
            <div class="point-style-1">
              <div class="pagination"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- VITA ---------------------------------------------------------------------------------------------------->
<div class="global-wrapper">
  <div id="about-us">
    <div class="container-fluid no-padd">
      <div class="row section no-bottom-padd no-marg">
        <div class="col-sm-12 no-padd">
          <div class="second-title ">
            <p>Vita</p>
            <h2>Walentina Wachtel</h2>
          </div>
          <div class="service-block right-side ">
            <div class="block-60">
              <div class="clip">
                <div class="bg bg-bg act">
                  <img src="images/vita_01_walentina-wachtel.jpg" class="s-img-switch" alt="">
                </div>
              </div>
              <div class="layer-white"></div>
            </div>
            <div class="block-40">
              <div class="text">
                <!-- <h4>Teaser</h4>-->
                <p class="text-p">…ist 1989 in Russland geboren und begann ihre Klavierausbildung mit fünf Jahren an der Spezialschule für Musik in Krasnojarsk bei der Pianistin und Klavierpädagogin Irina Wachtel. 1997 siedelte ihre Familie nach Deutschland über, wo sie ihre musikalische Ausbildung an der Musikschule in Lutherstadt Wittenberg fortsetzte. Ab 2006 erhielt sie Unterricht bei Prof. Josef Christof an der Musikhochschule in Leipzig und 2009 begann sie ihr Studium an der Hochschule für Musik „Carl Maria von Weber“ in Dresden bei Prof. Ute Pruggmayer-Philipp, ab 2013 bei Prof. Gunar Nauck. </p>
              </div>
            </div>
          </div>
          <div class="service-block left-side ">
            <div class="block-60">
              <div class="clip">
                <div class="bg bg-bg act">
                  <img src="images/vita_02_walentina-wachtel.jpg" class="s-img-switch" alt="">
                </div>
              </div>
              <div class="layer-white"></div>
            </div>
            <div class="block-40">
              <div class="text">
                <!-- <h4>Teaser</h4>-->
                <p class="text-p">Sie ist mehrfache Preisträgerin des Bundeswettbewerbs "Jugend musiziert“. Außerdem errang sie Preise beim Bachwettbewerb in Köthen, beim Robert-Schumann-Wettbewerb und nahm beim Internationalen Dostal-Operettenwettbewerb in Wien als Korrepetitorin teil. 2009 erhielt Sie den Förderpreis des Rotary-Clubs Zerbst und den Jugendförderpreis vom Mendelssohn-Haus Leipzig. Von 2012 bis 2014 war sie Mitglied in der Liedklasse bei Kammersänger Prof. Olaf Bär und Prof. Ulrike Siedel. Sie bestritt Meisterkurse bei Prof. Gregor Weichert, Prof. Anatol Ugorski, Andreas Staier, Prof. Günter Philipp und Gerold Huber.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- FOTOS ---------------------------------------------------------------------------------------------------->
  <div id="portfolio" class="container-fluid portfolio_bg">
    <div class="container section no-padd">
      <div class="second-title ">
        <p>Bilder</p>
        <h2>Bühnenimpressionen</h2>
      </div>
      <div class="portfolio-items">

        <div class="izotope-container impression-gallery">
          <div class="grid-sizer"></div>
          <div class="item height width_370">
            <a href="images/galerie_01.jpg"><img src="images/galerie_01_small.jpg" alt="">
              <div class="item-layer"></div>
            </a>
          </div>
          <div class="item height width_370">
            <a href="images/galerie_02.jpg"><img src="images/galerie_02_small.jpg" alt="">
              <div class="item-layer"></div>
            </a>
          </div>
          <div class="item height width_370">
            <a href="images/galerie_03.jpg"><img src="images/galerie_03_small.jpg" alt="">
              <div class="item-layer"></div>
            </a>
          </div>
          <div class="item height width_370">
            <a href="images/galerie_04.jpg"><img src="images/galerie_04_small.jpg" alt="">
              <div class="item-layer"></div>
            </a>
          </div>
          <div class="item height width_370">
            <a href="images/galerie_05.jpg"><img src="images/galerie_05_small.jpg" alt="">
              <div class="item-layer"></div>
            </a>
          </div>
          <div class="item height width_770">
            <a href="images/galerie_06.jpg"><img src="images/galerie_06_small.jpg" alt="">
              <div class="item-layer"></div>
            </a>
          </div>

        </div>

      </div>
    </div>
  </div>

  <!-- AUDIO ---------------------------------------------------------------------------------------------------->
  <div id="why-we">
    <div class="container-fluid">
      <div class="row row-no-padding">
        <div class="col-sm-12 no-padd">
          <div class="choose">
            <div class="left-block">
              <div class="clip">
                <div class="bg bg-bg"></div>
              </div>
              <div class="layer-white"></div>
            </div>
            <div class="right-block">
              <div class="vertical-align">
                <div class="title">
                  <p>Audio</p>
                  <h2> „Die Musik drückt das aus, was nicht gesagt werden kann und worüber zu schweigen unmöglich ist.“ - Victor Hugo</h2>
                </div>
                <div class="block-50">
                  <div class="choose-block">
                    <div class="choose-icon bg-main">

                    </div>
                    <div class="choose-txt">

                      <p>Die junge Musikerin fühlt sich auf jeder Bühne zuhause. Neben einer Vielzahl von Auftritten in Sachsen, Sachsen- Anhalt, Thüringen, Brandenburg, Berlin und Bayern war sie musikalisch auch schon in Österreich, Tschechien, Ungarn und Russland unterwegs. Sie spielte unter anderem im Gewandhaus zu Leipzig, eröffnete mit einem Solokonzert die Fasch-Festtage in Anhalt-Zerbst und musizierte für Politiker wie Ministerpräsident von Sachsen-Anhalt, Dr. Reiner Haseloff. Genauso aber auch umrahmte Sie mit Ihrem Klavierspiel zahlreiche Firmenfeiern, Hochzeiten sowie private Veranstaltungen. Zu Ihrem Repertoire gehören nicht nur anspruchsvolle Werke der Klassik, Sie begeistert sich auch für Filmmusik, Jazz und Klassikpop. </p>
                    </div>
                  </div>
                  <div class="choose-block">
                    <div class="choose-icon bg-main">

                    </div>
                    <div class="choose-txt">

                      <p></p>
                    </div>
                  </div>
                </div>
                <div class="block-50">
                  <div class="choose-block">
                    <div class="choose-icon bg-main">
                      <i class="fa fa-music" aria-hidden="true"></i>
                    </div>
                    <div class="choose-txt">
                      <h6>Nikolai Medtner - Märchen, Op. 51, Nr. 1 (Live)</h6>
                      <p>
                        <audio id="audio_with_controls" controls src="audio/nikolai-medtner-maerchen-op-51-Nr-1.mp3" type="audio/mp3">
                          Ihr Browser kann dieses Tondokument nicht wiedergeben.<br> Sie können es unter
                          <a href="audio/nikolai-medtner-maerchen-op-51-Nr-1.mp3">diesem Link</a> abrufen.
                        </audio>

                      </p>
                    </div>
                  </div>
                  <div class="choose-block">
                    <div class="choose-icon bg-main">
                      <i class="fa fa-music" aria-hidden="true"></i>
                    </div>
                    <div class="choose-txt">
                      <h6>Domenico Scarlatti - Sonate in d-Moll, K. 1, L. 366 (Live)</h6>
                      <p>
                        <audio id="audio_with_controls" controls src="audio/domenico-scarlatti-sonate-in-d-moll-k1-l366.mp3" type="audio/mp3">
                          Ihr Browser kann dieses Tondokument nicht wiedergeben.<br> Sie können es unter
                          <a href="audio/domenico-scarlatti-sonate-in-d-moll-k1-l366.mp3">diesem Link</a> abrufen.
                        </audio>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- MUSIKPÄDAGOGIK ---------------------------------------------------------------------------------------------------->
  <div id="services" class="container-fluid no-padd">
    <div class="row section no-bottom-padd no-marg">
      <div class="col-sm-12 no-padd">
        <div class="second-title">
          <p>Klavierunterricht</p>
          <h2>„Die Erziehung zur Musik ist von höchster Wichtigkeit, weil Rhythmus und Harmonie machtvoll in das Innerste der Seele dringen.“ - Platon</h2>
        </div>
        <div class="service-block right-side ">
          <div class="block-60 block-50">
            <div class="clip">
              <div class="bg bg-bg act">
                <img src="images/klavier_03_walentina-wachtel.jpg" class="s-img-switch" alt="">
              </div>
            </div>
            <div class="layer-white"></div>
          </div>
          <div class="block-50 block-40">
            <div class="text">
              <h4>Leidenschaft</h4>
              <h6>als Klavierpädagogin</h6>
              <p class="text-p">Neben Ihrer Leidenschaft zum Musizieren, möchte Walentina Wachtel auch das Unterrichten keinesfalls missen. Sie hat bereits im Studium erste Erfahrungen als Klavierpädagogin gesammelt. Sie unterrichtet derweilen an der Musikschule „Johann Sebastian Bach“ in Leipzig, der Kreismusikschule Lutherstadt Wittenberg und der Kreismusikschule Bautzen.</p>
            </div>
          </div>
        </div>
        <div class="service-block left-side ">
          <div class="block-60 block-50">
            <div class="clip">
              <div class="bg bg-bg act">
                <img src="images/klavier_02_walentina-wachtel.jpg" class="s-img-switch" alt="">
              </div>
            </div>
            <div class="layer-white"></div>
          </div>
          <div class="block-50 block-40">
            <div class="text">
              <h4>Energie</h4>
              <h6>für individuelle Projekte</h6>
              <p class="text-p">Energisch und voller Freude ist Sie an der musikalischen sowie persönlichen Entwicklung Ihrer Schüler interessiert und engagiert sich für individuelle Projekte Ihrer Klavierschüler, die im Alter von 4 bis 80 Jahren sind. Schon im Studium legte Sie großen Wert auf ein ganzkörperliches Unterrichtsprinzip und belegte ihre Theorie mit einer wissenschaftlichen Arbeit über „Körperbezogene Ansätze im frühen Klavierunterricht“. </p>
            </div>
          </div>
        </div>
        <div class="service-block right-side ">
          <div class="block-60 block-50">
            <div class="clip">
              <div class="bg bg-bg act">
                <img src="images/klavier_01_walentina-wachtel.jpg" class="s-img-switch" alt="">
              </div>
            </div>
            <div class="layer-white"></div>
          </div>
          <div class="block-50 block-40">
            <div class="text">
              <h4>Rhythmik</h4>
              <h6>als Musikpädagogin</h6>
              <p class="text-p">Ihr Zweitfach Rhythmik im Studium bei Prof. Christine Straumer und Meisterkurse am „Institut Jaques-Dalcroze“ in Belgien prägten Sie sehr in Ihrer methodischen und didaktischen Herangehensweise als Musikpädagogin. </p>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>


  <!-- DUO WALIA ---------------------------------------------------------------------------------------------------->
  <div id="testimonials" class="container-fluid">
    <div class="row section testimonials_bg">
      <div class="container no-padd">
        <div class="second-title ">
          <p>Duo Walia</p>
          <h2>Gesang und Klavier</h2><br><br><br> <span class="duo">
                  Das Duo WALIA existiert seit 2010.
                  <br><br>
                  Im Studium haben sich die zwei jungen Musikerinnen gefunden und neben einer engen Freundschaft <br>
                  auch eine unverzichtbare musikalische Bindung aufleben lassen. <br>
                  Die Opernsängerin <a href="http://www.juliadomke.de/" target="_blank"><strong>Julia Domke</strong></a> absolvierte, genauso wie Walentina Wachtel, <br>
                  Ihre klassische Ausbildung an der Hochschule für Musik in Dresden.
                  <br><br>
                  Die Musikerinnen haben im Studium Unterricht von Kammersänger Prof. Olaf Bär und Prof. Ulrike Siedel erhalten  <br>
                  und waren von 2012 bis 2014 Mitglieder in der Liedklasse der Hochschule, <br>
                  wo sie Ihre musikalischen Fähigkeiten im Zusammenspiel weiter ausbauten und Konzerterfahrungen als Duo sammelten.
                  <br><br>
                  Sie haben auch bei Wettbewerben Ihr musikalisches Können unter Beweis gestellt <br>
                  und beim Internationalen Dostal Operetten Wettbewerb in Wien 2012 den vierten Platz belegt. <br>
                  Neben einer Vielzahl öffentlicher Auftritte, darunter das Neujahrskonzert für die Stadt Bautzen 2013, <br>
                  musizieren die Künstlerinnen auch bei privaten musikalischen Engagements.
                  <br><br>
                  Durch das klassische Studium sind sie natürlich sehr vertraut mit Oper, Operette, Musical und Kunstlied, <br>
                  tragen aber genauso auf Wunsch chansonartige Poplieder vor. <br>
                  Zu Ihrem Repertoire gehören neben Werken in der deutschen Sprache, <br>
                  auch italienische, französische, tschechische und russische Werke.
                    </span>
        </div>


        <div class="container section no-padd">

          <div class="portfolio-items">

            <div class="izotope-container walia-gallery">
              <div class="grid-sizer"></div>

              <div class="item height width_370">
                <a href="images/duo-walia_05.jpg"><img src="images/duo-walia_05_small.jpg" alt="">
                  <div class="item-layer"></div>
                </a>
              </div>


              <div class="item height width_770">
                <a href="images/duo-walia_06.jpg"><img src="images/duo-walia_06_small.jpg" alt="">
                  <div class="item-layer"></div>
                </a>
              </div>

              <div class="item width_370">
                <a href="images/duo-walia_02.jpg"><img src="images/duo-walia_02_small.jpg" alt="">
                  <div class="item-layer"></div>
                </a>
              </div>

              <div class="item height width_370">
                <a href="images/duo-walia_03.jpg"><img src="images/duo-walia_03_small.jpg" alt="">
                  <div class="item-layer"></div>
                </a>
              </div>


              <div class="item height width_370">
                <a href="images/duo-walia_01.jpg"><img src="images/duo-walia_01_small.jpg" alt="">
                  <div class="item-layer"></div>
                </a>
              </div>


              <div class="item height width_370">
                <a href="images/duo-walia_04.jpg"><img src="images/duo-walia_04_small.jpg" alt="">
                  <div class="item-layer"></div>
                </a>
              </div>


            </div>

          </div>
        </div>

        <div class="second-title duo-mp3 ">
          <div class="choose-icon bg-main">
            <i class="fa fa-music" aria-hidden="true"></i>
          </div>
          <p>Alexander Zemlinsky - Walzer-Gesänge, Op. 6, Liebe Schwalbe (Live)</p>
          <audio id="audio_with_controls" controls src="audio/alexander-zemlinsky-walzer-gesaenge-op6-liebe-schwalbe.mp3" type="audio/mp3">
            Ihr Browser kann dieses Tondokument nicht wiedergeben.<br> Sie können es unter
            <a href="audio/alexander-zemlinsky-walzer-gesaenge-op6-liebe-schwalbe.mp3">diesem Link</a> abrufen.
          </audio>

        </div>


        <div class="swiper-anime">
          <div class="swiper-container customer-slider" data-autoplay="0" data-loop="0" data-speed="1000" data-center="0" data-slides-per-view="responsive" data-xs-slides="1" data-sm-slides="2" data-md-slides="3" data-lg-slides="3" data-add-slides="3">
            <div class="swiper-wrapper">

              <div class="swiper-slide">
                <div class="cust-block cust-top">
                  <div class="text bg-main"><span class="quote">“</span>
                    <p>Am Instrument wirkte eine bestens mit dem Metier vertraute Begleiterin, die jede Ausdrucksschattierung der spritzigen Sopranistin auffing.</p>
                    <span>Sächsische Zeitung </span>
                  </div>
                </div>
              </div>

              <div class="swiper-slide">
                <div class="cust-block cust-bottom">
                  <div class="text bg-main"><span class="quote">“</span>
                    <p>Die auf toskanischen Volksliedern basierenden Walzer-Gesänge von Alexander von Zemlinsky wurden von der Vogtländerin
                      <a href="http://www.juliadomke.de/" target="_blank">Julia Domke</a> mehr artifiziell denn volkstümlich gesungen, wiewohl sie in der Russland-Deutschen Walentina Wachtel eine profunde Begleiterin hatten.
                    </p><span>Dresdener Neuste Nachrichten </span>
                  </div>
                </div>
              </div>

              <div class="swiper-slide">
                <div class="cust-block cust-top">
                  <div class="text bg-main"><span class="quote">“</span>
                    <p>Ihre überaus große Stimme fand die rechte Saalakustik und erfreute Herz und Sinne.</p>
                    <span>Sächsische Zeitung </span>
                  </div>
                </div>
              </div>

            </div>
            <div class="pagination"></div>
          </div>


        </div>
      </div>
    </div>

  </div>
  <div id="contact-us" class="container-fluid">
    <div class="container">
      <div class="row section no-bottom-padd">
        <div class="col-sm-12">
          <div class="second-title ">
            <p>Kontakt</p>
            <h2>So können Sie mich kontaktieren</h2><br><br>
          </div>
        </div>
      </div>
      <div class="row margin-lg-55b">
        <div class="col-sm-6">

          <div role="form" class="mt-contact" id="mt-contact-f166-o1" lang="de-DE" dir="ltr">

            <form action="" method="post" class="mt-contact-form" name="form" id="form">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <span class="name">
                    <input type="text" name="name" aria-invalid="false" placeholder="Name" value="<?=$_POST['name']?>" required />
                  </span>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <span class="email">
                    <input type="email" name="email" aria-invalid="false" placeholder="Email" value="<?=$_POST['email']?>" required />
                  </span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <span class="message">
                    <textarea name="nachricht" cols="40" rows="10" aria-invalid="false" placeholder="Ihre Nachricht" required><?=$_POST['nachricht']?></textarea>
                  </span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                  <img src="<?=$_SESSION['captcha']['image_src']?>">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <span class="name">
                    Bitte hier den Code eingeben.
                    <input type="text" name="captcha" placeholder="Captcha" required />
                  </span>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 text-right">
                  <div class="submit-wrapper">
                    <input type="submit" name="submit" value="absenden" />
                  </div>
                </div>
              </div>
            </form>

          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 ajax-result">
            <div class="success"></div>
          </div>

        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <div class="contact-block">
                <div class="choose-icon bg-second"><i class="pe-7s-phone"></i></div>
                <div class="text">
                  <h6>Telefon</h6>
                  <p><a href="tel:">+49 176 433 413 15</a></p>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="contact-block">
                <div class="choose-icon bg-second"><i class="pe-7s-mail-open-file"></i></div>
                <div class="text">
                  <h6>EMail</h6>
                  <p><a href="mailto:">Walentina.Wachtel@gmail.com</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <div class="contact-block">
                <div class="choose-icon bg-second"><i class="pe-7s-map-2"></i></div>
                <div class="text">
                  <h6>Adresse</h6>
                  <p>Walentina Wachtel <br> Weiße Gasse 2<br> 01067 Dresden</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="contact-block">
                <div class="choose-icon bg-second"><i class="pe-7s-clock"></i></div>
                <div class="text">
                  <h6>Telefonisch erreichbar</h6>
                  <p>24 | 7</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer>
    <div class="container">
      <div class="footer">
        <div class="folow margin-zerro">
          <!-- Button trigger modal -->
          <button type="button" class="btn_impressum" data-toggle="modal" data-target="#myModal">
            Impressum
          </button>

          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Impressum</h4>
                </div>
                <div class="modal-body">
                  Angaben gemäß § 5 TMG<br><br>

                  Walentina Wachtel <br> Weiße Gasse 2 <br> 01067 Dresden <br>

                  <br><br>


                  <h2>Kontakt:</h2><br>
                  <p>Telefon: +49 176 433 413 15</p>
                  <p> E-Mail: Walentina.Wachtel@gmail.com </p>

                  <br><br>

                  <h2>Haftungsausschluss:</h2><br>

                  <br><br>Haftung für Inhalte<br><br>

                  <p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>

                  <br><br>Haftung für Links<br><br>

                  <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>

                  <br><br>Urheberrecht<br><br>

                  <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>

                  <br><br> Datenschutz<br><br>

                  <p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben. Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich. Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor. </p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Zurück</button>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright">
          <div class="copy">Walentina Wachtel © 2017</div>
        </div>
      </div>
    </div>
  </footer>
</div>
<script src='libs/jquery-3.1.1.min.js'></script>
<script src="libs/jquery-migrate-3.0.0.min.js"></script>
<script src='libs/jquery.fitvids.min.js'></script>
<script src='libs/anchors.nav.min.js'></script>
<script src='libs/idangerous.swiper.min.js'></script>
<script src="libs/imagesloaded.min.js"></script>
<script src='libs/jquery.magnific-popup.min.js'></script>
<script src='libs/isotope.pkgd.min.js'></script>
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyA6M45oe9V8IfJfUB6x4k0FKhmEf58nJAs&#038;'></script>
<script src='libs/equalHeightsPlugin.min.js'></script>
<script src='libs/jquery.lettering.min.js'></script>
<script src='libs/jquery.textillate.min.js'></script>
<script src='libs/jquery.countTo.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src='js/all.js'></script>
<script src='js/switcher.js'></script>
</body>

</html>