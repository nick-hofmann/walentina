<?php
/**
 * Created by PhpStorm.
 * User: Mario
 * Date: 31.08.2017
 * Time: 12:08
 */

$email_from = "info@walentinawachtel.de";
$email_to = "walentina.wachtel@gmail.com";

session_start();

if ($_POST['name'] != "" && $_POST['email'] != "" && $_POST['nachricht'] != "") {

  if (strtolower($_SESSION['captcha']['code']) == strtolower($_POST['captcha'])) {
    $_POST['name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
    $_POST['email'] = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $_POST['nachricht'] = filter_var($_POST['nachricht'], FILTER_SANITIZE_STRING);
    $nachricht = "Name: ".$_POST['name']."<br>";
    $nachricht .= "E-Mail: ".$_POST['email']."<br>";
    $nachricht .= "Nachricht: ".$_POST['nachricht']."<br>";
    $nachricht = nl2br($nachricht);
    $nachricht = wordwrap($nachricht, 70);

    $subject = utf8_decode("Anfrage von Website - ".$_POST['name']);

    require 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host = 'starwebserver.de';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@walentinawachtel.de';
    $mail->Password = 'B:vxLr2S+2kV';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom($email_from);
    $mail->addAddress($email_to);
    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body = utf8_decode($nachricht);
    $mail->AltBody = utf8_decode($nachricht);

    if (!$mail->send()) {
      $str_error .= "<b>Interner Fehler</b> Ihre Daten konnten nicht versand werden!\n";
      echo $mail->ErrorInfo;
    } else {
      echo 'Vielen Dank für Ihre Anfrage.';
    }
  } else {
    echo 'Falscher Captchacode!';
  }
}