;(function($, window, document, undefined) {
	"use strict";

	/*--------------------------------------------------------------------------------------*/
// background img
/*--------------------------------------------------------------------------------------*/

function wpc_add_img_bg(img_sel, parent_sel) {

	if (!img_sel) {
		console.info('no img selector');
		return false;
	}
	var $parent, _this;

	$(img_sel).each(function() {
		_this = $(this);
		$parent = _this.closest(parent_sel);
		$parent = $parent.length ? $parent : _this.parent();
		$parent.css('background-image', 'url(' + this.src + ')');
		_this.hide();
	});

}
wpc_add_img_bg('.wpc-img-style a img', '.wpc-img-style a');
$('.conf-button').on('click', function() {
        if ($('.wpc-style-page').hasClass('slide-right')) {
            $('.wpc-style-page').removeClass('slide-right');
            $('.conf-button span').removeClass('act');
        } else {
            $('.wpc-style-page').addClass('slide-right');
            $('.conf-button span').addClass('act');
        }

    });

    /*--------------------------------------------------------------------------------------*/
    // change colors fonts 
    /*--------------------------------------------------------------------------------------*/

document.querySelector("body").className += " "+localStorage.getItem("color");

    $('.entry').on('click', function() {
        var newTheme = $(this).attr('data-color');
        if ($(this).hasClass('active')) return false;
        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
        $('body').removeClass().addClass(newTheme)

        localStorage.removeItem("color");
        localStorage.setItem("color", $(this).attr("data-color"));
    });



/*--------------------------------------------------------------------------------------*/
// change block container
/*--------------------------------------------------------------------------------------*/



    $('.check-option').on('click', function() {
        $('body').removeClass().removeAttr('style');

        if ($(this).hasClass('active')) {
            return false;
        } else {
            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');
            $('html').removeClass();
            if ($('.wpc-product-text .col-sm-6').length) {
                $('.wpc-product-text .wpc-product-item').parent().removeClass().addClass('col-sm-12');
            } else {
                $('.wpc-product-text .wpc-product-item ').parent().removeClass().addClass('col-sm-6');
            }
            $('html').addClass($(this).attr('data-size'));
            initSwiper();
        }
    });




/*--------------------------------------------------------------------------------------*/
// change background container
/*--------------------------------------------------------------------------------------*/
$('.wpc-img-style a').on('click', function(e) {
    e.preventDefault();
    var that = $(this);
    that.parent().children().removeClass('active')
    that.addClass('active')
    $('.wpc-product-text .wpc-product-item').parent().removeClass().addClass('col-sm-12');
    $('html').addClass('wpc-no-fluid').addClass('wpc-bg-body').css('background', $(this).attr('data-bg-color') || 'url(' + $(this).find('img').attr('src')  + ')');
    $('.check-option').removeClass('active').eq(0).addClass('active');
    $('body').css('background', '#fff');

})

if ($('.wpc-img-style .wpc-bg-color').length) {
   $('.wpc-img-style .wpc-bg-color').each(function() {
       $(this).css('background',   $(this).attr('data-bg-color')  );
   });
}


/*--------------------------------------------------------------------------------------*/
/* Hide scroll in block
/*--------------------------------------------------------------------------------------*/
var div = document.createElement('div');

div.style.overflowY = 'scroll';
div.style.width = '50px';
div.style.height = '50px';
div.style.visibility = 'hidden';

document.body.appendChild(div);
var scrollWidth = div.offsetWidth - div.clientWidth;

$('.wrappers').css('margin-right', -scrollWidth);
document.body.removeChild(div);


})(jQuery, window, document);