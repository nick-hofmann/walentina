;
(function($, window, document, undefined) {
    "use strict";

    // video
    var $video = $("#video");
    if ($video.length) {
        $.getScript('libs/jquery.youtubebackground.min.js', function() {
            //postpone execution not the end of execution queue.
            if ($(window).width() >= 1200) {
                setTimeout(function() {
                    $video.YTPlayer({
                        fitToBackground: true,
                        videoId: 'LCFGcXqdJBg',
                        playerVars: {
                            modestbranding: 0,
                            autoplay: 1,
                            controls: 0,
                            showinfo: 0,
                            branding: 0,
                            rel: 0,
                            autohide: 1,
                            start: 0
                        }
                    });
                }, 0);
            }
        });
    }

    $('.container').fitVids();

    /*============================*/
    /* SWIPER SLIDE */
    /*============================*/

    var swipers = [],
        winW, winH, winScr, _isresponsive, smPoint = 768,
        mdPoint = 992,
        lgPoint = 1200,
        addPoint = 1600,
        _ismobile = navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i);

    function pageCalculations() {
        winW = $(window).width();
        winH = $(window).height();
    }

    pageCalculations();
    //  notFound
    function notFound() {
        if ($('body.error404').length) {
            var minus = 32;
            var $notFaund = $('.not-founded');
            if ($(window).width() < 782) {
                minus = 46;
            }
            $notFaund.height($(window).height() - $('footer').height() - minus);
            $notFaund.width($(window).width());
        }
    }
    //  menuArrows
    function menuArrows() {
        if ($(window).width() > 991) {
            if (!$('header.top-menu ul.sub-menu > li.menu-item-has-children > a > .fa-angle-left').length) {
                $('header.top-menu ul.sub-menu > li.menu-item-has-children > a').prepend('<span class="fa fa-angle-left"></span>');
            }
        } else {
            $('ul.sub-menu > li.menu-item-has-children > a .fa-angle-left').remove();
        }
        if ($(window).width() < 993) {
            if (!$('header.left-menu ul > li.menu-item-has-children > a > .fa-angle-down').length) {
                $('header.left-menu ul > li.menu-item-has-children > a').append('<span class="fa fa-angle-down"></span>');
            }
        }
    }
    // imageWidth
    function imageWidth() {
        var $bgBg = $('.bg-bg-chrome');
        if ($bgBg.length) {
            $bgBg.width($('.bg-bg-chrome').closest('.clip').width());
        }
    }
    // showHeader
    function showHeader() {
        if (!$('.simple-article-block').length) {
            var $header = $('header');
            if ($(window).scrollTop() >= 1) {
                $header.addClass('scrol');
            } else {
                $header.removeClass('scrol');
            }
        }
    }
    // headerPosition
    function headerPosition() {
        var $adminBar = $('.admin-bar header');
        if ($(window).width() < 601) {
            if ($(window).scrollTop() < 47) {
                $adminBar.css({
                    'top': 46 - $(window).scrollTop()
                });
            } else {
                $adminBar.css({
                    'top': 0
                });
            }
        } else {
            if ($(window).width() < 769) {
                $adminBar.css({
                    'top': '46px'
                })
            } else {
                $adminBar.css({
                    'top': '32px'
                })
            }
        }
    }

    if (_ismobile) {
        var $fitVid = $('.fitvidsignore');
        if ($fitVid.length) {
            $fitVid.each(function() {
                var $parent = $(this).closest('.top-baner');
                var $poster = $parent.data('poster');
                $parent.css({
                    'background-image': 'url(' + $poster + ')'
                });
                $(this).remove();
            });
        }
    }


    $('.comment-widget input[type="submit"]').wrap('<div class="sub-wrap"></div>');

    function sampleHeight() {
        $('.info-block').css('height', 'auto').equalHeights();

        if ($(window).width() > 991) {
            $('.posts-wrapper, .sidebar').css('height', 'auto').equalHeights();
        }
    }

    sampleHeight();


    function portfolio() {
        if ($(window).width() < 1280) {

            $(".izotope-container .item").each(function(index) {
                var urlImg = $(this).find('a').attr('href');
                $(this).css('background', 'url(' + urlImg + ')');
            });

        }
    }
    portfolio();


    /*============================*/
    /* WIDGET SEARCH */
    /*============================*/


    $('.widget.widget_search input[type="search"]').attr('placeholder', 'Type...');

    /*============================*/
    /* WINDOW LOAD */
    /*============================*/


    $(window).on('load', function() {
        menuArrows();
        sampleHeight();

        initSwiper();

        notFound();
        imageWidth();
        headerPosition();
        showHeader();

        wpc_add_img_bg('.s-img-switch', '.s-back-switch');


        var $canvas = $('.map-canvas');
        if ($canvas.length) {
            $canvas.each(function() {
                initialize(this);
            });
        }

        $('#loading').hide();

        var $container = $('.izotope-container');
        if ($container.length) {

            $container.imagesLoaded(function() {
                $container.isotope({
                    itemSelector: '.item',
                    layoutMode: 'masonry',
                    masonry: {
                        columnWidth: '.grid-sizer',
                        isFitWidth: true
                    }
                });
            });
        }
        if ($('.simple-article-block').length) {
            $('header').addClass('scrol');
        }

    });

    function updateSlidesPerView(swiperContainer) {
        if (winW >= addPoint) return parseInt(swiperContainer.attr('data-add-slides'), 10);
        else if (winW >= lgPoint) return parseInt(swiperContainer.attr('data-lg-slides'), 10);
        else if (winW >= mdPoint) return parseInt(swiperContainer.attr('data-md-slides'), 10);
        else if (winW >= smPoint) return parseInt(swiperContainer.attr('data-sm-slides'), 10);
        else return parseInt(swiperContainer.attr('data-xs-slides'), 10);
    }

    function resizeCall() {
        pageCalculations();
        $('.swiper-container.initialized[data-slides-per-view="responsive"]').each(function() {
            var thisSwiper = swipers['swiper-' + $(this).attr('id')],
                $t = $(this),
                slidesPerViewVar = updateSlidesPerView($t),
                centerVar = thisSwiper.params.centeredSlides;
            thisSwiper.params.slidesPerView = slidesPerViewVar;
            thisSwiper.reInit();
            if (!centerVar) {
                var paginationSpan = $t.find('.pagination span');
                var paginationSlice = paginationSpan.hide().slice(0, (paginationSpan.length + 1 - slidesPerViewVar));
                if (paginationSlice.length <= 1 || slidesPerViewVar >= $t.find('.swiper-slide').length) $t.addClass('pagination-hidden');
                else $t.removeClass('pagination-hidden');
                paginationSlice.show();
            }
        });
    }
    if (!_ismobile) {
        $(window).resize(function() {
            resizeCall();
        });
    } else {
        window.addEventListener("orientationchange", function() {
            resizeCall();
        }, false);
    }

    /*=====================*/
    /* 07 - swiper sliders */
    /*=====================*/

    function initSwiper() {
        var initIterator = 0;

        $('.swiper-container').each(function() {
            var $t = $(this);
            var count_item = $t.find('.swiper-slide').length;
            var index = 'swiper-unique-id-' + initIterator;

            $t.find('.swiper-slide').each(function(l, i) {
                $(this).attr('data-val', l);
            });

            if (!$t.hasClass('style-3')) {

                var index = 'swiper-unique-id-' + initIterator;

                $t.addClass('swiper-' + index + ' initialized').attr('id', index);
                $t.find('.pagination').addClass('pagination-' + index);

                var autoPlayVar = parseInt($t.attr('data-autoplay'), 10);

                var mode = $t.attr('data-mode');

                var slidesPerViewVar = $t.attr('data-slides-per-view');
                if (slidesPerViewVar === 'responsive') {
                    slidesPerViewVar = updateSlidesPerView($t);
                } else slidesPerViewVar = parseInt(slidesPerViewVar, 10);

                var loopVar = parseInt($t.attr('data-loop'), 10);
                var speedVar = parseInt($t.attr('data-speed'), 10);
                var centerVar = parseInt($t.attr('data-center'), 10);
                swipers['swiper-' + index] = new Swiper('.swiper-' + index, {
                    speed: speedVar,
                    pagination: '.pagination-' + index,
                    loop: loopVar,
                    paginationClickable: true,
                    autoplay: autoPlayVar,
                    slidesPerView: slidesPerViewVar,
                    keyboardControl: true,
                    calculateHeight: true,
                    simulateTouch: true,
                    roundLengths: true,
                    centeredSlides: centerVar,
                    mode: mode || 'horizontal',
                    onInit: function(swiper) {
                        $t.find('.swiper-slide').addClass('active');
                        var activeIndex = (loopVar === 1) ? swiper.activeLoopIndex : swiper.activeIndex;
                    },
                    onSlideChangeEnd: function(swiper) {
                        var activeIndex = (loopVar === 1) ? swiper.activeLoopIndex : swiper.activeIndex;
                        var qVal = $t.find('.swiper-slide-active').attr('data-val');
                        $t.find('.swiper-slide[data-val="' + qVal + '"]').addClass('active');
                    },
                    onSlideChangeStart: function(swiper) {
                        $t.find('.swiper-slide.active').removeClass('active');
                        var activeIndex = (loopVar === 1) ? swiper.activeLoopIndex : swiper.activeIndex;
                    }
                });
                swipers['swiper-' + index].reInit();
                if ($t.attr('data-slides-per-view') === 'responsive') {
                    var paginationSpan = $t.find('.pagination span');
                    var paginationSlice = paginationSpan.hide().slice(0, (paginationSpan.length + 1 - slidesPerViewVar));
                    if (paginationSlice.length <= 1 || slidesPerViewVar >= $t.find('.swiper-slide').length) $t.addClass('pagination-hidden');
                    else $t.removeClass('pagination-hidden');
                    paginationSlice.show();
                }
                initIterator++;
            }
        });

    }

    //swiper arrows

    $('.swiper-arrow-left').on('click', function() {
        swipers['swiper-' + $(this).closest('.swiper-anime').find('.swiper-container').attr('id')].swipePrev();
    });

    $('.swiper-arrow-right').on('click', function() {
        swipers['swiper-' + $(this).closest('.swiper-anime').find('.swiper-container').attr('id')].swipeNext();
    });

    $('.swiper-outer-left').on('click', function() {
        swipers['swiper-' + $(this).parent().find('.swiper-container').attr('id')].swipePrev();
    });

    $('.swiper-outer-right').on('click', function() {
        swipers['swiper-' + $(this).parent().find('.swiper-container').attr('id')].swipeNext();
    });

    /*============================*/
    /* DROPDOWN */
    /*============================*/


    $('.nav-menu-icon a').on('click', function() {
        var $nav = $('nav');
        if ($nav.hasClass('slide-menu')) {
            $nav.removeClass('slide-menu');
            $(this).removeClass('active');
            $('.navigation').css({
                'overflow': 'hidden'
            });
        } else {
            $nav.addClass('slide-menu');
            $(this).addClass('active');

            setTimeout(function() {
                $('.navigation').css({
                    'overflow': 'initial'
                });
            }, 500);
        }

        if (bodyOverf() <= 992) {
            if ($('nav').hasClass('slide-menu')) {
                $("body").addClass("overflow");
            } else {
                $("body").removeClass("overflow");
            }

        }
        return false;
    });

    $('.left-menu nav > ul > li').on('click', function() {
        $('.left-menu nav > ul > li > ul').toggleClass('vis');
    });

    $('.left-menu nav > ul > li > ul > li').on('click', function() {
        $('.left-menu nav > ul > li > ul > li > ul').toggleClass('vis');
    });

    $('.left-menu nav ul li a').on('click', function() {
        if (!$(this).parent().hasClass('menu-item-has-children')) {
            $("body").removeClass("overflow");
        }
    });




    function bodyOverf() {
        winW = $(window).width();
        return winW;
    }
    bodyOverf();



    $('.blog-main + .blog-post-item').addClass('first');

    /***********************************/
    /*WINDOW SCROLL*/
    /**********************************/



    $(window).on('scroll', function() {
        headerPosition();
        showHeader();


        var $timeLine = $('.time-line');
        if ($timeLine.length) {
            $timeLine.not('.animated').each(function() {
                $(this).find('.timer').countTo();

            });
        }

        if ($('.start-line').length) {
            $('.skill-line div').each(function() {
                if ($(window).scrollTop() >= $(this).offset().top - $(window).height() * 0.8) {
                    var objel = $(this);
                    var pb_width = objel.attr('data-width-pb');
                    objel.css({
                        'width': pb_width
                    });
                }
            });
        }

    });

    $('.up-button').on('click', function() {
        $('body, html').animate({
            'scrollTop': '0'
        });
        return false;
    });




    /***********************************/
    /* BACKGROUND*/
    /**********************************/

    //sets child image as a background
    function wpc_add_img_bg(img_sel, parent_sel) {

        if (!img_sel) {
            console.info('no img selector');
            return false;
        }

        var $parent, $imgDataHidden, _this;

        $(img_sel).each(function() {
            _this = $(this);
            $imgDataHidden = _this.data('s-hidden');
            $parent = _this.closest(parent_sel);
            $parent = $parent.length ? $parent : _this.parent();
            $parent.css('background-image', 'url(' + this.src + ')').addClass('s-back-switch');
            if ($imgDataHidden) {
                _this.css('visibility', 'hidden');
            } else {
                _this.css('visibility', 'hidden');
            }
        });

    }

    /***********************************/
    /*IZOTOPE*/
    /**********************************/

    var $container = $('.izotope-container');
    if ($container.length) {
        $container.imagesLoaded(function() {
            $container.isotope({
                itemSelector: '.item',
                layoutMode: 'masonry',
                masonry: {
                    columnWidth: '.grid-sizer'
                }
            });
        });
    }
    // getItemElements
    function getItemElements() {
        var $item = $('.item').slice(4).clone();
        return $item;
    }

    $('.load-more a').on('click', function(e) {
        e.preventDefault();
        var $count = $(this).attr('data-count');
        var $portfolioItems = $(this).closest('.portfolio-items').find('.izotope-container');


        $portfolioItems.find('.hidden-item').each(function(i, el) {

            if (i < $count) {
                var element = $(this).find('img');
                $(this).removeClass('hidden-item');
                element.attr('src', element.attr('data-src'));
            }

        });

        $portfolioItems.imagesLoaded(function() {
            $portfolioItems.isotope({
                itemSelector: '.item',
                layoutMode: 'masonry',
                masonry: {
                    columnWidth: '.grid-sizer',
                    isFitWidth: true
                }
            });
        });

        // setTimeout($portfolioItems.isotope(), 300);

        if (!$portfolioItems.find('.hidden-item').length) {
            $(this).fadeOut(300);
            $(this).closest('.portfolio-items').append('<div class="btn-no-more"><span class="no-more">no more projects</span></div>');
        }
    });

    $('#filters').on('click', '.but', function() {
        $('.izotope-container').each(function() {
            $(this).find('.item').removeClass('animated');
        });

        $('#filters .but').removeClass('activbut');
        $(this).addClass('activbut');
        var filterValue = $(this).attr('data-filter');
        $container.isotope({
            filter: filterValue
        });
    });


    /***********************************/
    /*GOOGLE MAP*/
    /**********************************/
    var $map = $('.wpc-map');
    if ($map.length) {
        $map.each(function() {
            init_map(this);
        });
    }

    function init_map(_this) {
        // Create a map object and specify the DOM element for display.
        var style, i, markers = [],
            marker,
            default_style = {
                'style-1': [{
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#000000"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#000000"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
                    }, {
                        "lightness": 17
                    }]
                }]
            },
            markersDone = [],
            zoom = parseInt($(_this).attr("data-zoom"), 10),
            markers_data = JSON.parse($(_this).attr("data-marker")),
            styleMap = $(_this).attr("data-style");

        // default style map
        if (styleMap === 'style-1') {
            style = default_style[styleMap];
        }

        // custom style map
        if (typeof marseille_style_map != 'undefined' && styleMap === 'custom') {
            style = marseille_style_map;
        }

        var infowindow = new google.maps.InfoWindow(), // infowindow
            myLatlng = new google.maps.LatLng(markers_data[0][0], markers_data[0][1]), // centering map
            styledMap = new google.maps.StyledMapType(style, {
                name: "Styled Map"
            }), // style map
            map = new google.maps.Map(_this, {
                zoom: zoom,
                disableDefaultUI: false,
                center: myLatlng,
                scrollwheel: false
            });

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        //add new markers
        for (i = 0; i < markers_data.length; i++) {

            // create new marker
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(markers_data[i][0], markers_data[i][1]),
                map: map,
                icon: markers_data[i][4]
            });

            //add marker to arrays
            markers.push(marker);

            // create infowindow
            infowindow = new google.maps.InfoWindow({
                content: markers_data[i][2]
            });

            // open info window
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });

        }

    }


    /***********************************/
    /*POPUP*/
    /**********************************/
    var $popUp = $('.impression-gallery');
    if ($popUp.length) {
        $popUp.magnificPopup({
            delegate: 'a',
            type: 'image',
            removalDelay: 300,
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',
                opener: function(openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });
    }

    var $popUp = $('.walia-gallery');
    if ($popUp.length) {
        $popUp.magnificPopup({
            delegate: 'a',
            type: 'image',
            removalDelay: 300,
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',
                opener: function(openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });
    }
    // aboutSize
    function aboutSize() {
        $(".block-about").imagesLoaded(function() {
            $(".block-about .img-wrap").each(function(index) {
                $(this).css({
                    'width': 'auto',
                    'height': 'auto'
                }).css({
                    'width': $(this).find('img').width() + 'px',
                    'height': $(this).find('img').height() + 'px'
                });
            });
        });
    }
    aboutSize();
    $(window).on('orientationchange', function() {
        aboutSize();
        portfolio();
    });

    $(window).on('resize', function() {
        sampleHeight();
        notFound();
        headerPosition();
        imageWidth();
        menuArrows();
        portfolio();
        aboutSize();
        bodyOverf();
        var $body = $("body");
        if ($(window).width() > 992) {
            $body.removeClass("overflow");
        } else if ($(window).width() <= 992 && $("nav").hasClass("slide-menu")) {
            $body.addClass("overflow");
        }
    });


    $(window).on('load', function() {

        $('.banner-post .banner-content h2.title, .blog-main').textillate({ in: {
                effect: 'flip',
                delay: 30
            }
        });
   
  

    });

    // form
    $('.mt-contact-form').on('submit', function(e) {

        $('.ajax-loader').show();

        var url = 'mail.php',
            form = this;

        /*$(form).find('[name="*').remove();*/

        function result(class_key, data) {
            setTimeout(function() {
                $('.ajax-loader').hide();
                $('.ajax-result').find(class_key).show().text(data);
                var string = data;
                var match = string.match(/vielen/i);
                if(match) resetForm($('form[name=form]'));
            }, 500);
        }

        function resetForm($form) {
            $form.find('input:text, textarea').val('');
            $('.mt-contact-form').hide();
        }

        $.ajax({
                type: "POST",
                url: url,
                data: $(form).serialize(),
            })
            .done(function(data) {
                result('.success', data);
            }).error(function(data) {
                result('.error', data);
            });



        e.preventDefault();

    });
})(jQuery, window, document);